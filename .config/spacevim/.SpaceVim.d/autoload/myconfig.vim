"=============================================================================
" My Custom Vim Settings
"=============================================================================
function! myconfig#after() abort
"=============================================================================
" Personal Settings
"=============================================================================
set wrap
set textwidth=80
set linebreak
set nobackup
set nohlsearch
set incsearch
set history=1000
set signcolumn=yes
set encoding=utf-8
set updatetime=100
" set cmdheight=2
set shortmess+=c
set scrolloff=8

"=============================================================================
" Custom Keybindings
"=============================================================================
nnoremap W :w
nnoremap C :Cheat<cr>
nnoremap <C-r> :Ranger<cr>
vnoremap <C-c> "+y<cr>
nnoremap <C-p> "+p<cr>
#nnoremap <C-v> :Vifm<cr>
nnoremap <C-w> :w<cr>
nnoremap <C-q> :q!<cr>
nnoremap <C-x> :wq<cr>

"=============================================================================
" Sneak
"=============================================================================
map s <Plug>Sneak_s
map S <Plug>Sneak_S

"=============================================================================
" Custom Autocommands
"=============================================================================
fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

augroup FOSS_KING
  autocmd!
  autocmd BufWritePre * :call TrimWhitespace()
augroup END

:echo exists('##TextYankPost')

"=============================================================================
"FZF
"=============================================================================
"This is setting Alt/meta-f to be used easily on it.
execute "set <M-f>=\ef"

map <m-f> :Files<CR>
map <C-h> :History<CR>
map <C-g> :RG<CR>
map <C-s> :BLines<CR>
map <C-a> :Lines<CR>
map <C-b> :Buffers<CR>
nnoremap <leader>g :Rg<CR>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>m :Marks<CR>
let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline'
let $FZF_DEFAULT_COMMAND="rg --files --hidden"
"Get Files
command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'options': ['--layout=reverse', '--info=inline']}), <bang>0)


" Get text in files with Rg
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(), <bang>0)

" Ripgrep advanced
function! RipgrepFzf(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

" Git grep
command! -bang -nargs=* GGrep
  \ call fzf#vim#grep(
  \   'git grep --line-number '.shellescape(<q-args>), 0,
  \   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

"=============================================================================
endfunction
"=============================================================================
" More Custom Vim Settings
"=============================================================================
function! myconfig#before() abort
"=============================================================================
" Airline
"=============================================================================
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='dracula'
let g:airline_section_x = '%{ScrollStatus()} '
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1

"=============================================================================
" ScrollStatus
"=============================================================================
let g:scrollstatus_size = 12
let g:scrollstatus_symbol_track = '-'
let g:scrollstatus_symbol_bar = '|'

"=============================================================================
" Vim Rainbow
"=============================================================================
let g:rainbow_active = 1

"=============================================================================
" Startify
"=============================================================================
let g:startify_custom_indices = map(range(10,100), 'string(v:val)')
let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Recently Opened Files']            },
          \ { 'type': 'sessions',  'header': ['   My Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   My Bookmarks']      },
          \ ]
let g:startify_bookmarks = [
            \ { 'm': '~/.SpaceVim.d/autoload/myconfig.vim' },
            \ { 'I': '~/.SpaceVim.d/init.toml'},
            \ { 's': '~/.zshrc'},
            \ { 'f': '~/.config/fish/config.fish'},
            \ { 't': '~/test.sh'},
            \ ]
let g:webdevicons_enable_startify = 1

"=============================================================================
" Moloki-Dark
"=============================================================================
let g:molokaidark_undercolor_gui = 1
let g:molokaidark_undercolor_cterm = 0

"=============================================================================
" FZF
"=============================================================================
" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'
let g:fzf_tags_command = 'ctags -R'
" Border color
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }
" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

"=============================================================================
"=============================================================================
endfunction
