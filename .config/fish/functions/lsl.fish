function lsl --wraps='exa -T -L=2 -a -B -h -l --icons' --description 'alias lsl=exa -T -L=2 -a -B -h -l --icons'
  exa -T -L=2 -a -B -h -l --icons $argv; 
end
