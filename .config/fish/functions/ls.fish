function ls --wraps='exa -T -L=1 -a -B -h -l --icons' --description 'alias ls=exa -T -L=1 -a -B -h -l --icons'
  exa -T -L=1 -a -B -h -l --icons $argv; 
end
