function vpn --wraps='suod protonvpn c -f' --wraps='sudo protonvpn c -f' --description 'alias vpn=sudo protonvpn c -f'
  sudo protonvpn c -f $argv; 
end
