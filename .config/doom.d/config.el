(setq user-full-name "MDBDEVIO")

(setq doom-theme 'doom-one)
;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

(setq bookmark-default-file "~/.dotfiles/doom.d/bookmarks")

(map! :leader
      (:prefix ("b". "buffer")
       :desc "List bookmarks"                          "L" #'list-bookmarks
       :desc "Set bookmark"                            "m" #'bookmark-set
       :desc "Delete bookmark"                         "M" #'bookmark-set
       :desc "Save current bookmarks to bookmark file" "w" #'bookmark-save))

(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)

(evil-define-key 'normal ibuffer-mode-map
  (kbd "f c") 'ibuffer-filter-by-content
  (kbd "f d") 'ibuffer-filter-by-directory
  (kbd "f f") 'ibuffer-filter-by-filename
  (kbd "f m") 'ibuffer-filter-by-mode
  (kbd "f n") 'ibuffer-filter-by-name
  (kbd "f x") 'ibuffer-filter-disable
  (kbd "g h") 'ibuffer-do-kill-lines
  (kbd "g H") 'ibuffer-update)

;; If you use `org' and don't want your org iles in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Emacs/1.Org")

;; CUSTOM TODO states
(after! org
(setq org-todo-keywords
      '((sequence "TODO(t)"
         "PLANNING(p)"
         "IN-PROGRESS(i)"
         "WEEKLY-GOAL(m)"
         "GOAL(g)"
         "WAITING(w)"
         "WORK(b)"
         "HABIT(h)"
         "PROJECT(P)"
         "CALENDAR(c)"
         "|"
         "HOLD(h)"
         "SOMEDAY(s)"
         "WONT-DO(n)"
         "DONE(d!)")
        )))

;; CUSTOM TODO colors
(after! org
(setq org-todo-keyword-faces
      '(
        ("TODO" . (:foreground "gold" :weight bold))
        ("PLANNING" . (:foreground "dark violet" :weight bold))
        ("IN-PROGRESS" . (:foreground "DarkOrange" :weight bold))
        ("WEEKLY-GOAL" . (:foreground "light sea green" :weight bold))
        ("GOAL" . (:foreground "LimeGreen" :weight bold))
        ("WAITING" . (:foreground "LightPink1" :weight bold))
        ("WORK" . (:foreground "Cyan" :weight bold))
        ("HABIT" . (:foreground "RoyalBlue3" :weight bold))
        ("WONT-DO" . (:foreground "Brown2" :weight bold))
        ("SOMEDAY" . (:foreground "cyan1" :weight bold))
        ("HOLD" . (:foreground "Grey46" :weight bold))
        ("Project" . (:foreground "SlateBlue1" :weight bold))
        ("CALENDAR" . (:foreground "chocolate" :weight bold))
        ("DONE" . (:foreground "white" :weight bold))
        )))

;; Tag colors
(setq org-tag-faces
      '(
        ("planning"  . (:foreground "mediumPurple1" :weight bold))
        ("@research"   . (:foreground "royalblue1"    :weight bold))
        ("QA"        . (:foreground "sienna"        :weight bold))
        ("CRITICAL"  . (:foreground "red1"          :weight bold))
        ("HABIT"  . (:foreground "pink"          :weight bold))
        )
      )

;;Creates a nice work log for me to capture to
(after! org
  (setq org-capture-templates
        '(
;; Add to inbox
          ("i" "INBOX"
        entry (file+datetree "~/Emacs/1.Org/INBOX.org" "INBOX")
         "* TODO %?"
         :empty-lines 0)
        ("n" "Personal Notes/Scatch Pad"
         entry (file+headline "~/Emacs/1.Org/NOTES.org" "Personal Notes")
         "** %?"
         :empty-lines 0)
;; To create work todos
        ("w" "Work-Todo"
         entry (file+datetree "~/Emacs/1.Org/WORK_TODO.org" "Work-TODO")
         "* WORK %?"
         :empty-lines 0)
;; To create work notes
        ("W" "Work-Notes"
         entry (file+datetree "~/Emacs/1.Org/WORK_TODO.org" "Work-Notes")
         "* NOTE %?"
         :empty-lines 0)
;; To create achievments todos
        ("a" "Achievments"
         entry (file+datetree "~/Emacs/1.Org/1.NOTES/1.Personal/2.AchNGrat/ACHIEVMENTS.org" "Achievments")
          "* %?"
          :empty-lines 0)
;; Add to Gratitude Diary
        ("g" "Gratidude Diary"
         entry (file+datetree "~/Emacs/1.Org/1.NOTES/1.Personal/2.AchNGrat/GRATITUDE.org" "Gratitude Diary")
          "* %?"
          :empty-lines 0)
;; Daily Review
        ("d" "Daily Review"
         entry (file+datetree "~/Emacs/1.Org/1.NOTES/1.Personal/3.Reviews/1.Daily/1.DailyReviews.org" "Daily Reviews")
         "* Daily Review: %?

 ** What outcomes did I achieve today?

  1.

 ** How did these contribute to my larger outcomes?*

  1.

 ** What made me happiest today?*

   -

 ** What have I given today?*

   -

 ** What did I learn today?*

   -

 ** How has today added to the quality of my life or how can I use today*

   -

 ** What could I have improved upon and done better?*

   -

 ** What action or plan can I put in place to improve the above moving forward?*

   -

 ** Who have I been in service of today?*

   -


 ** What is one small thing that is special about me?*

   -

 ** What did I do well today?*

   -

 ** Total of focused minutes? Or notes on hacking.*"
          :empty-lines 0)

 ;; Weekly Reviews
        ("R" "Weekly Review"
         entry (file+datetree "~/Emacs/1.Org/1.NOTES/1.Personal/3.Reviews/2.Weekly/1.WeeklyReviews.org" "Weekly Reviews")
         "* Weekly Review: %?


 ** Three things that are going well?

   1.


 ** Three things that could improve?

   1.


 ** What is one small thing I can do to improve the above?

   -

 ** Note Completed outcomes/Goals (or not) and explain why.


   1.

        - Complete or not: Complete

        - How/why not?

	- Can I replicate this moving forward if so how, if not why not?

        - Notes:

   2.

        - Complete or not: Complete

        - How/why not?

	- Can I replicate this moving forward if so how, if not why not?

        - Notes:


   3.

        - Complete or not: Complete

        - How/why not?

	- Can I replicate this moving forward if so how, if not why not?

        - Notes:




 ** Thoughts on the week? A few lines will suffice."

          :empty-lines 0)
       )))

(require 'org)

(defun org-select-recur-task ()
  (let ((days-of-week
         '(("Monday" . "1")
           ("Tuesday" . "2")
           ("Wednesday" . "3")
           ("Thursday" . "4")
           ("Friday" . "5")
           ("Saturday" . "6")
           ("Sunday" . "7")))
        (org-recur-days)
        (selected-day))
    (while (not (string= selected-day "finish"))
      (setq selected-day (completing-read "Select days to recur the task (type 'finish' to complete): " (append days-of-week '(("finish")) ) nil t))
      (when (and (not (string= selected-day "finish")) (not (string= selected-day "")))
        (let ((day-code (cdr (assoc selected-day days-of-week))))
          (unless (member day-code org-recur-days)
            (setq org-recur-days (append org-recur-days (list day-code)))))))
    org-recur-days))

(defun org-recur-setup ()
  (interactive)
  (let* ((org-recur-days (org-select-recur-task))
         (recurring-days (concat "+"
                                 (string-join org-recur-days ",")
                                 "d"))
         (current-time (format-time-string "%Y-%m-%d %a")))
    (end-of-line)
    (newline)
    (insert (format "DEADLINE: <%s %s>" current-time recurring-days))
    (setq org-deadline-warning-days 0)))

(define-key org-mode-map (kbd "C-c r") 'org-recur-setup)

(defun org-calculate-next-deadline (current-date recurring-days)
  (let* ((current-day (string-to-number (format-time-string "%u" current-date)))
         (recurrence-pattern (mapcar #'string-to-number (split-string recurring-days ",")))
         (next-day (seq-find (lambda (day) (> day current-day)) (sort recurrence-pattern '<))))
    (if next-day
        (org-read-date nil t (concat "+" (number-to-string (- next-day current-day)) "d"))
      (org-read-date nil t (concat "+" (number-to-string (- (apply '+ recurrence-pattern) current-day)) "d")))))

(defun org-renew-recurring-task ()
  (when (string= org-state "DONE")
    (let* ((deadline (org-entry-get nil "DEADLINE"))
           (recurring-days (when deadline (string-match "\\+\\(.*\\)d>" deadline) (match-string 1 deadline)))
           (next-deadline (when recurring-days (org-calculate-next-deadline (current-time) recurring-days))))
      (when next-deadline
        (org-deadline nil (format "<%s +%sd>" (format-time-string "%Y-%m-%d %a" next-deadline) recurring-days))
        (org-todo "TODO")))))

(add-hook 'org-after-todo-state-change-hook 'org-renew-recurring-task)

;;;;;;;;;;;;;;;;;;;;;ORG CRYPT
;; ORG CRYPT TAG Setup for inline encryption
;; If I place "crypt" tag in any entry it will encrypt it.
(after! org
(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance '("crypt"))
;; GPG key to use for encryption
;; Either the Key ID or set to nil to use symmetric encryption.
(setq org-crypt-key "79EA004594BD7E09")
;; Set shortut to decrypt easier.
(map! :leader
      :desc "Org Decrypt Entry"
      "d e" #'org-decrypt-entry))

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-include-deadlines t
     ;; org-agenda-block-separator nil
     ;;   org-agenda-compact-blocks t
      org-agenda-start-day nil ;; i.e. today
      org-agenda-span 1
      org-agenda-start-on-weekday nil)
  (setq org-agenda-custom-commands
        '(("c" "Super view"
                     ((agenda "" ((org-agenda-span 'day)
                      (org-super-agenda-groups
                       '((:name "⏰⏰⏰⏰⏰ --- Today --- ⏰⏰⏰⏰⏰"
                          :discard (:todo "DONE")
                          :discard (:tag "habit")
                          :time-grid t
                          :date today
                          :todo "TODAY"
                          :scheduled today
                          :discard (:anything)
                          :order 1)))))
                      (alltodo "" ((org-agenda-overriding-header "CURRENT STATUS")
                                   (org-agenda-prefix-format "  %t  %s")
                         (org-super-agenda-groups
                          '((:log t)
                            (:name "⚠⚠⚠ --- MISSION CRITICAL TASKS --- ⚠⚠⚠"
                             :discard (:todo "DONE")
                             :tag "CRITICAL"
                             :order 2
                             :transformer (--> it
                                  (upcase it)
                                  (propertize it 'face '(:foreground "red1"))))
                            (:name " 🚧🚧🚧 --- ACTIVE PROJECT(s) --- 🚧🚧🚧 "
                             :todo "PROJECT"
                             :order 6
                             :transformer (--> it
                                  (upcase it)
                                  (propertize it 'face '(:foreground "SlateBlue1"))))
                            (:name "〰️〰️〰 --- Currently Working On --- 〰〰〰"
                                   :todo "IN-PROGRESS"
                                   :order 4)
                            (:name "❗❗❗ --- Important --- ❗❗❗"
                                   :discard (:todo "DONE")
                                   :priority "A"
                                   :date today
                                   :order 10)
                            (:name "✅✅✅ --- GOAL --- ✅✅✅"
                                   :todo "GOAL"
                                   :order 2
                                   :transformer (--> it
                                        (upcase it)
                                        (propertize it 'face '(:foreground "LimeGreen"))))
                            (:name "✅✅✅ --- WEEKLY-GOALS --- ✅✅✅"
                                   :todo "WEEKLY-GOAL"
                                   :order 3
                                   :transformer (--> it
                                        (upcase it)
                                        (propertize it 'face '(:foreground "light sea green"))))
                            (:name "❌⚠❌ --- Overdue! --- ❌⚠❌"
                                   :discard (:todo "DONE")
                                   :deadline past
                                   :scheduled past
                                   :transformer (--> it
                                        (upcase it)
                                        (propertize it 'face '(:foreground "red")))
                                   :order 5)
                            (:name " 🏃‍🏃‍‍🏃 --- Errands --- 🏃‍🏃‍‍🏃"
                                   :and (:scheduled today :tag "Errands")
                                   :order 12)
                            (:name "⏲⏲⏲ --- Waiting --- ⏲⏲⏲"
                                   :todo "WAITING"
                                   :order 6)
                            (:name "🇧🇧🇧 --- WORK --- 🇧🇧🇧"
                                   :and (:scheduled today :tag "WORK" :todo "WORK")
                                   :order 9)
                            (:name "📅 📅 📅 --- Calendar Events --- 📅 📅 📅"
                                   :and (:scheduled today :todo "CALENDAR")
                                   :order 19)
                            (:name "✔✔✔ --- HABIT --- ✔✔✔"
                                   :and (:scheduled today :tag "habit")
                                   :transformer (--> it
                                        (upcase it)
                                        (propertize it 'face '(:foreground "royalblue1")))
                                   :order 20)
                           (:discard (:anything))))))))))
  :config
  (org-super-agenda-mode))

;; Journal Config
(setq org-journal-dir "~/Emacs/1.Org/1.NOTES/1.Personal/1.Journal/"
      org-journal-date-prefix "#+TITLE: "
      org-journal-time-prefix "* "
      org-journal-date-format "%a, %d-%m-%Y"
      org-journal-file-format "%d-%m-%Y-jrnl.org")

;; Preview images in all org files on launch
(setq org-startup-with-inline-images t)
(beacon-mode 1)

;; Used to open specific commonly used files
(map! :leader
      (:prefix ("=" . "open file")
       :desc "Edit TODO File" "t" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/TODO.org"))
       :desc "Edit INBOX File" "i" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/INBOX.org"))
       :desc "Edit WORK File" "w" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/WORK_TODO.org"))
       :desc "Edit Repeating File" "r" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/REPEATING.org"))
       :desc "Edit Hacking TODO File" "h" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/HACKING.org"))
       :desc "Hacking Best tools Doc" "b" #'(lambda () (interactive) (find-file "~/Emacs/1.Org/1.NOTES/3.Hacking/__BestTools/1.BestTools.org"))))
(map! :leader
      (:prefix ("= d" . "Open Doom Files")
       :desc "Edit Doom config.el"   "c" #'(lambda () (interactive) (find-file "~/.dotfiles/doom.d/README.org"))
       :desc "Edit Doom init.el "   "i" #'(lambda () (interactive) (find-file "~/.dotfiles/doom.d/init.el"))
       :desc "Edit Doom packages.el "   "p" #'(lambda () (interactive) (find-file "~/.dotfiles/doom.d/packages.el"))))

;; Enables auto tangling/exporting of code blocks to a unified code file form org mode.
;; It means I can jsut write code blocks in org with detailed documentation and this will export it all accordingly.
(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))

;; Hide emphasis markers in text this means that MD and org syntax icons will not show
(after! org
(setq org-hide-emphasis-markers t))

;;Customize ORG higlighting
;; this controls the color of bold, italic, underline, verbatim, strikethrough
(after! org
(setq org-emphasis-alist
  '(("*" (bold :slant italic :weight black :foreground "DeepSkyBlue" )) ;; this make bold both italic and bold, but not color change
    ("/" (italic :foreground "dark salmon" )) ;; italic text, the text will be "dark salmon"
    ("_" underline :foreground "cyan" ) ;; underlined text, color is "cyan"
    ("=" (:background "PaleGreen1" :foreground "dim gray" )) ;; background of text is "snow1" and text is "deep slate blue"
    ("~" (:background "PaleGreen1" :foreground "dim gray" ))
    ("+" (:strike-through nil :foreground "dark orange" )))))

(use-package! org-modern
  :hook (org-mode . org-modern-mode)
  :config
  (setq
   ;; Edit settings
   org-auto-align-tags nil
   org-tags-column 0
   org-catch-invisible-edits 'show-and-error
   org-special-ctrl-a/e t
   org-insert-heading-respect-content t
   ;; Appearance
   org-modern-radio-target    '("❰" t "❱")
   org-modern-internal-target '("↪ " t "")
   org-modern-todo nil
   org-modern-tag nil
   org-modern-timestamp t
   org-modern-statistics nil
   org-modern-progress nil
   org-modern-priority nil
   org-modern-horizontal-rule "──────────"
   org-modern-hide-stars "·"
   org-modern-star ["⁖"]
   org-modern-keyword "‣"
   org-modern-list '((43 . "•")
                     (45 . "–")
                     (42 . "∘")))
 ;; Org styling, hide markup etc.
  org-hide-emphasis-markers t
  org-pretty-entities t

  ;; Agenda styling
 org-agenda-tags-column 0
 org-agenda-block-separator ?─
 org-agenda-time-grid
 '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
 org-agenda-current-time-string
 "⭠ now ─────────────────────────────────────────────────")


  (custom-set-faces!
    `((org-modern-tag)
      :background ,(doom-blend (doom-color 'blue) (doom-color 'bg) 0.1)
      :foreground ,(doom-color 'grey))
    `((org-modern-radio-target org-modern-internal-target)
      :inherit 'default :foreground ,(doom-color 'blue)))

;; Open neotree with (SPC t n) open dir with (SPC d n)
(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))
(after! doom-themes
  (setq doom-neotree-enable-variable-pitch t))
;(map! :leader
 ;     :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
  ;    :desc "Open directory in neotree" "d n" #'neotree-dir)

;; dired customizaion

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "Z") 'dired-do-compress
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-do-kill-lines
  (kbd "% l") 'dired-downcase
  (kbd "% m") 'dired-mark-files-regexp
  (kbd "% u") 'dired-upcase
  (kbd "* %") 'dired-mark-files-regexp
  (kbd "* .") 'dired-mark-extension
  (kbd "* /") 'dired-mark-directories
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))

(define-key evil-motion-state-map (kbd "C-h") #'evil-window-left)
(define-key evil-motion-state-map (kbd "C-j") #'evil-window-down)
(define-key evil-motion-state-map (kbd "C-k") #'evil-window-up)
(define-key evil-motion-state-map (kbd "C-l") #'evil-window-right)

(defun my/resize-window (direction amount)
  "Resizes the window in the specified DIRECTION by AMOUNT lines or columns."
  (interactive)
  (let ((func (pcase direction
                ('h #'evil-window-decrease-width)
                ('l #'evil-window-increase-width)
                ('j #'evil-window-decrease-height)
                ('k #'evil-window-increase-height))))
    (funcall func amount)))

(global-set-key (kbd "C-c h") (lambda () (interactive) (my/resize-window 'h 5)))
(global-set-key (kbd "C-c l") (lambda () (interactive) (my/resize-window 'l 5)))
(global-set-key (kbd "C-c j") (lambda () (interactive) (my/resize-window 'j 5)))
(global-set-key (kbd "C-c k") (lambda () (interactive) (my/resize-window 'k 5)))

(defun my/increase-text-height ()
  (interactive)
  (text-scale-increase 1))

(defun my/decrease-text-height ()
  (interactive)
  (text-scale-decrease 1))

(global-set-key (kbd "C-=") 'my/increase-text-height)
(global-set-key (kbd "C--") 'my/decrease-text-height)

(setq treemacs-width 30)
(setq treemacs--width-is-locked nil)
(setq treemacs-width-is-initially-locked nil)

(map! :leader
      :desc "Toggle Treemacs file viewer" "t n" #'+treemacs/toggle)

;; Enables Emofis
(use-package emojify
  :hook (after-init . global-emojify-mode))

(defun dt/insert-todays-date (prefix)
  (interactive "P")
  (let ((format (cond
                 ((not prefix) "%A, %B %d, %Y")
                 ((equal prefix '(4)) "%m-%d-%Y")
                 ((equal prefix '(16)) "%Y-%m-%d"))))
    (insert (format-time-string format))))

(require 'calendar)
(defun dt/insert-any-date (date)
  "Insert DATE using the current locale."
  (interactive (list (calendar-read-date)))
  (insert (calendar-date-string date)))

(map! :leader
      (:prefix ("i d" . "Insert date")
        :desc "Insert any date"    "a" #'dt/insert-any-date
        :desc "Insert todays date" "t" #'dt/insert-todays-date))

(setq centaur-tabs-set-bar 'over
      centaur-tabs-set-icons t
      centaur-tabs-gray-out-icons 'buffer
      centaur-tabs-height 24
      centaur-tabs-set-modified-marker t
      centaur-tabs-style "bar"
      centaur-tabs-modified-marker "•")
(map! :leader
      :desc "Toggle tabs globally" "t c" #'centaur-tabs-mode
      :desc "Toggle tabs local display" "t C" #'centaur-tabs-local-mode)
(evil-define-key 'normal centaur-tabs-mode-map (kbd "g <right>") 'centaur-tabs-forward        ; default Doom binding is 'g t'
                                               (kbd "g <left>")  'centaur-tabs-backward       ; default Doom binding is 'g T'
                                               (kbd "g <down>")  'centaur-tabs-forward-group
                                               (kbd "g <up>")    'centaur-tabs-backward-group)

(beacon-mode 1)

;; Calendar
; https://stackoverflow.com/questions/9547912/emacs-calendar-show-more-than-3-months
(defun dt/year-calendar (&optional year)
  (interactive)
  (require 'calendar)
  (let* (
      (current-year (number-to-string (nth 5 (decode-time (current-time)))))
      (month 0)
      (year (if year year (string-to-number (format-time-string "%Y" (current-time))))))
    (switch-to-buffer (get-buffer-create calendar-buffer))
    (when (not (eq major-mode 'calendar-mode))
      (calendar-mode))
    (setq displayed-month month)
    (setq displayed-year year)
    (setq buffer-read-only nil)
    (erase-buffer)
    ;; horizontal rows
    (dotimes (j 4)
      ;; vertical columns
      (dotimes (i 3)
        (calendar-generate-month
          (setq month (+ month 1))
          year
          ;; indentation / spacing between months
          (+ 5 (* 25 i))))
      (goto-char (point-max))
      (insert (make-string (- 10 (count-lines (point-min) (point-max))) ?\n))
      (widen)
      (goto-char (point-max))
      (narrow-to-region (point-max) (point-max)))
    (widen)
    (goto-char (point-min))
    (setq buffer-read-only t)))

(defun dt/scroll-year-calendar-forward (&optional arg event)
  "Scroll the yearly calendar by year in a forward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (unless arg (setq arg 0))
  (save-selected-window
    (if (setq event (event-start event)) (select-window (posn-window event)))
    (unless (zerop arg)
      (let* (
              (year (+ displayed-year arg)))
        (dt/year-calendar year)))
    (goto-char (point-min))
    (run-hooks 'calendar-move-hook)))

(defun dt/scroll-year-calendar-backward (&optional arg event)
  "Scroll the yearly calendar by year in a backward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (dt/scroll-year-calendar-forward (- (or arg 1)) event))

(map! :leader
      :desc "Scroll year calendar backward" "<left>" #'dt/scroll-year-calendar-backward
      :desc "Scroll year calendar forward" "<right>" #'dt/scroll-year-calendar-forward)

(defalias 'year-calendar 'dt/year-calendar)

(use-package! calfw)
(use-package! calfw-org)

;; Markdown & line settings

(setq display-line-numbers-type t)
(map! :leader
      :desc "Comment or uncomment lines" "TAB TAB" #'comment-line
      (:prefix ("t" . "toggle")
       :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
       :desc "Toggle line highlight in frame" "h" #'hl-line-mode
       :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
       :desc "Toggle truncate lines" "t" #'toggle-truncate-lines))

(custom-set-faces
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "variable-pitch"))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.7))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.6))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.5))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :height 1.3))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :height 1.2)))))

(defun dt/insert-todays-date (prefix)
  (interactive "P")
  (let ((format (cond
                 ((not prefix) "%A, %B %d, %Y")
                 ((equal prefix '(4)) "%m-%d-%Y")
                 ((equal prefix '(16)) "%Y-%m-%d"))))
    (insert (format-time-string format))))

(require 'calendar)
(defun dt/insert-any-date (date)
  "Insert DATE using the current locale."
  (interactive (list (calendar-read-date)))
  (insert (calendar-date-string date)))

;; Uses clippty explain functionaility.
(map! :leader
      (:prefix ("c h" . "Help info from Clippy")
       :desc "Clippy describes function under point" "f" #'clippy-describe-function
       :desc "Clippy describes variable under point" "v" #'clippy-describe-variable))

;; EWW Web Browser
(setq browse-url-browser-function 'eww-browse-url)
(map! :leader
      :desc "Search web for text between BEG/END"
      "s w" #'eww-search-words
      (:prefix ("e" . "evaluate/ERC/EWW")
       :desc "Eww web browser" "w" #'eww
       :desc "Eww reload page" "R" #'eww-reload))
