#!/bin/bash

# Get the IP address from the user
read -p "Enter the IP address: " ip

# Extract the first three octets of the IP address
prefix=$(echo $ip | cut -d '.' -f 1-3)

# Loop through each IP address in the /24 range
for i in {1..254}
do
    # Construct the IP address to ping
    target="$prefix.$i"

    # Ping the target IP address
    ping -c 1 -W 1 $target > /dev/null

    # Check the exit code of the ping command
    if [ $? -eq 0 ]; then
        echo "$target is up"
    else
        echo "$target is down"
    fi
done
