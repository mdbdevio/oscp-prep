#!/bin/bash
#
# Get the script directory and name
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
script_name="$(basename "${BASH_SOURCE[0]}")"

# Ask the user for the input file path
read -p "Enter the input file path: " input_file
if [[ $input_file != /* ]]; then
    input_file="${script_dir}/${input_file}"
fi

# Define the output file path
output_file=""

# Define the list of response codes to filter for
response_codes=("200" "300" "400" "500")

# Ask the user to select the response codes to filter for
echo "Which response code(s) do you want to filter for?"
echo "Select one or more options (separated by spaces) and press ENTER to continue:"
select response_code in "${response_codes[@]}"; do
    case $response_code in
        "200")
            start_code=200
            end_code=299
            break;;
        "300")
            start_code=300
            end_code=399
            break;;
        "400")
            start_code=400
            end_code=499
            break;;
        "500")
            start_code=500
            end_code=599
            break;;
        *)
            echo "Invalid option. Please select one or more options from the list."
            ;;
    esac
done

# Construct the output file path
output_file="${input_file%.*}_filtered_${start_code}-${end_code}.txt"
output_file="${script_dir}/${output_file##*/}"

# Use grep to filter the input file for the specified response code(s) and write it to the output file
grep -E "^${start_code:0:1}[0-9]{2}" "$input_file" > "$output_file"
echo "Filtered lines written to: ${output_file}"
