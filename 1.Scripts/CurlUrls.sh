#!/usr/bin/env sh


# All LFI urls will need to be placed into a file called urls.txt in the same directory as the script. The script will then curl all urls and output to files to be gone through.
# If not using insecure (http) you can remove the `--insecure` clause from curl

# Prompt the user for the base domain
read -p "Enter the base domain: " base_domain

# Read list of URLs from urls.txt file
urls_file="urls.txt"

# Remove everything from the file except the results found between ""
sed -i 's/[^"]*"\([^"]*\)".*/\1/' urls.txt

# Check if the fule is there.
if [ ! -f "$urls_file" ]; then
  echo "Error: $urls_file not found"
  exit 1
fi
urls=($(cat "$urls_file"))

# Create output directory if it doesn't exist
mkdir -p curled_url_output

# Loop through each URL
for url in "${urls[@]}"
do
  # Concatenate the base domain with the path in the URL
  full_url="${base_domain}${url}"

  # Replace symbols in the URL with underscores
  file_name=$(echo $full_url | tr '.\/\\' '___')

  # Curl the URL and write the output to a file
  curl --insecure -Ls $full_url -o curled_url_output/$file_name.txt

  # Print the URL to the console
  echo "Output file created for ${full_url}"
done
